#ifndef LOGIC_H
#define LOGIC_H

#include <vector>
#include <QList>
#include <QByteArray>
#include <QAbstractListModel>
#include <QMessageBox>


enum Roles
{
    Value = Qt::UserRole + 1
};

struct Parameters
{
    const int cellsNumber;
    const int width;
    const int height;
    const int lastCellIndex;
    const int lastCellValue;
    const int initZeroIndex;
};

class Logic: public QAbstractListModel
{
    Q_OBJECT

public:
    explicit Logic(QObject *parent = 0);
    ~Logic();

    Q_PROPERTY(int zeroIndex MEMBER m_zeroIndex CONSTANT)

    Q_INVOKABLE bool    move(int index);
    Q_INVOKABLE void    shuffle();
    Q_INVOKABLE bool    cellsInRightOrder();

private:
    const Parameters m_params = {16, 4, 4, 15, 16, 14};
    QList<int>       m_cells;
    int              m_zeroIndex;

    void    moveCells(int from, int to);
    bool    checkSolving();

protected:
    int                    rowCount(const QModelIndex &parent) const override;
    QVariant               data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray> roleNames() const override;

};

#endif // LOGIC_H
