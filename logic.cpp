#include "logic.h"
#include <QDebug>

Logic::Logic(QObject *parent) :
    QAbstractListModel(parent)
{
    for (int i = 1; i < m_params.cellsNumber - 1; ++i)
        m_cells << i;
    m_cells << 0 << m_params.lastCellValue - 1;
    m_zeroIndex = m_params.initZeroIndex;
}

Logic::~Logic()
{
}

bool Logic::move(int index)
{
    bool isCloseToZero = abs(m_zeroIndex - index) == m_params.width ||
            (abs(m_zeroIndex - index) == 1 &&
             m_zeroIndex / m_params.height == index / m_params.height &&
             m_cells[m_zeroIndex] != m_params.lastCellValue);

    if (isCloseToZero) {
        moveCells(index, m_zeroIndex);
    }
    return isCloseToZero;

}

void Logic::shuffle()
{
    if (m_cells.at(m_params.lastCellIndex) == m_params.cellsNumber) {
        m_cells[m_params.lastCellIndex] = 0;
        emit dataChanged(createIndex(m_params.lastCellIndex, 0),
                         createIndex(m_params.lastCellIndex, 0));
    }
    do {
        for (int i = m_params.cellsNumber; i; --i)
        {
            int newPosition = rand() % m_params.cellsNumber;
            if (newPosition == i - 1)
                continue;
            moveCells(i - 1, newPosition);
        }
    } while(checkSolving());
    m_zeroIndex = m_cells.indexOf(0);
}

void Logic::moveCells(int from, int to)
{
    int  destination = from < to ? to + 1 : to;
    bool verticalMove = from % m_params.width == to % m_params.width;

    beginMoveRows(QModelIndex(), from, from, QModelIndex(), destination);
    m_cells.move(from, to);
    endMoveRows();

    if (verticalMove)
    {
        to += from <= to ? -1 : 1;
        destination = to < from ? from + 1 : from;
        beginMoveRows(QModelIndex(), to, to, QModelIndex(), destination);
        m_cells.move(to, from);
        endMoveRows();
    }
    m_zeroIndex = from;
}

bool Logic::checkSolving()
{
    int sum = 0;

    for (int i = 0; i < m_params.cellsNumber; ++i) {
        int number1 = m_cells.at(i);

        if (number1 == 0) {
            sum += i % m_params.height + 1;
            continue;
        }
        for (int j = i + 1; j < m_params.cellsNumber; ++j) {
            int number2 = m_cells.at(j);

            if (number2 < number1 && number2 != 0) {
                ++sum;
            }
        }
    }
    return (sum % 2 != 1);
}

bool Logic::cellsInRightOrder()
{
    bool finished = true;

    for (int i = 0; i < m_params.cellsNumber - 1; ++i) {
        if (m_cells[i] != i + 1) {
            finished = false;
            break;
        }
    }
    if (finished) {
        m_cells[m_zeroIndex] = m_params.lastCellValue;
        emit dataChanged(createIndex(m_params.lastCellIndex, 0),
                         createIndex(m_params.lastCellIndex, 0));
    }
    return finished;
}

int Logic::rowCount(const QModelIndex & ) const {
    return m_params.cellsNumber;
}

QVariant Logic::data(const QModelIndex & modelIndex, int role) const {
    if (!modelIndex.isValid()) {
        return QVariant();
    }

    int index = modelIndex.row();

    if (index >= m_params.cellsNumber || index < 0) {
        return QVariant();
    }

    switch (role) {
    case Roles::Value     : return m_cells.at(index);
    }
    return QVariant();
}

QHash<int, QByteArray> Logic::roleNames() const {
    QHash<int, QByteArray> names;
    names.insert(Roles::Value, "value");
    return names;
}
