import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Controls 1.4

ApplicationWindow {
    title: qsTr("Barley break")
    visible: true
    width: 200;
    height: 250

    property int cellsNumber: 16
    property int zeroIndex: 14
    property int squareWidth: width / 4
    property int squareHeight: (height - shuffleButton.height) / 4

    GridView {
        id: cellsGrid

        anchors.fill: parent
        cellWidth: squareWidth
        cellHeight: squareHeight

        model: logic

        delegate: Rectangle {
            id: rect

            width: squareWidth
            height: squareHeight
            scale: 0.9
            color: value > 0 ? "skyblue" : "transparent"

            Text {
                anchors.centerIn: parent
                font.pixelSize: parent.height / 3
                text: value > 0 ? value : ""
            }
            MouseArea {
                anchors.fill: parent

                onClicked: {
                    logic.move(index);
                    if (logic.cellsInRightOrder()) {
                        messageWon.open();

                    }
                }
            }
        }
        move: Transition {
            NumberAnimation {properties: "x,y"; duration: 1000;}
        }

    }
    Button {
        id: shuffleButton

        height: parent.height * 0.1
        width: parent.width * 0.4
        anchors {
            bottom: parent.bottom
            bottomMargin: 3
            horizontalCenter: parent.horizontalCenter
        }

        Text {
            text: "Shuffle"
            anchors.centerIn: parent
            font.pixelSize: parent.height * 0.5
        }

        onClicked:
        {
            logic.shuffle();
        }
    }

    MessageDialog {
        id: messageWon

        title: "Congratulations"
        text: "You've finished this game and all tiles are in right order:)"
        icon: StandardIcon.Information

        onAccepted: logic.shuffle();
    }
}
